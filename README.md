# ICST-Ambisonics-Plugin-Bundle #

> [!WARNING]  
> This repository is not maintained anymore. Versions up to 2.3 can still be downloaded from the Download section, but without any kind of warranty or support. Please find our new Repository at  [https://github.com/schweizerweb/icst-ambisonics-plugins](<https://github.com/schweizerweb/icst-ambisonics-plugins>)